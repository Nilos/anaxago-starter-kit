Test technique réalisé pour une entreprise
Anaxago symfony-starter-kit
===================

# Description

Ce projet est un kit de démarage avec :
- Symfony 3.4 minimum
- php 7.1 minimum

La base de données contient deux tables :
- user => pour la gestion et la connexion des utilisateurs 
- project => pour la liste des projets
- investment => marque d'intérêt des utilisateurs pour un projet

Les données préchargés sont
- pour les users 

| email     | password    | Role |
| ----------|-------------|--------|
| john@local.com  | john   | ROLE_USER    |
| admin@local.com | admin | ROLE_ADMIN   | 

 - une liste de 3 projets
 
La connexion et l'enregistrement des utilisateurs sont déjà configurés et opérationnels


# Installation
- ```composer install```
- ```composer init-db ```

    - Script personnalisé permet de créer la base de données, de lancer la création du schéma et de précharger les données
    - Ce script peut être réutilisé pour ré-initialiser la base de données à son état initial à tout moment

# Routes de l'API
- GET : http://localhost:8000/api/projects : Liste des projets
- POST : http://localhost:8000/api/investment : Marque d'intérêt d'un utilisateur pour un projet. Exemple de données : { "projectId":1, "amount":100000, "email":"john@local.com", "password":"john" }
- POST : http://localhost:8000/api/simulation-investment : { "length":10, "interestRate":10, "funding":"100000" }

# Améliorations à apporter
- Documentation de l'API
- Interface web
- Utilisation de Form dans l'API
