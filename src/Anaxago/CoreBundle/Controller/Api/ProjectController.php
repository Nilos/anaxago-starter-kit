<?php

namespace Anaxago\CoreBundle\Controller\Api;


use Anaxago\CoreBundle\Entity\Project;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\SerializerInterface;

class ProjectController extends Controller
{
    /**
     * Get all projects
     *
     * @Route("/api/projects")
     *
     */
    public function getAction(SerializerInterface $serializer) {

        $entityManager = $this->getDoctrine()->getManager();
        $projects = $entityManager->getRepository(Project::class)->findAll();

        $jsonContent = $serializer->serialize($projects, 'json');

        return new Response(utf8_encode($jsonContent), 200);

    }
}