<?php

namespace Anaxago\CoreBundle\Controller\Api;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class SimulationInvestmentController extends Controller
{
    /**
     * Simulate an investment
     *
     * @Route("/api/simulation-investment")
     * @Method("POST")
     * @param Request $request
     * @return Response
     */
    public function simulateInvestmentAction(Request $request) {

        // post data sent
        $length = $request->request->get('length', 0);
        $interestRate = $request->request->get('interestRate', 0);
        $funding = $request->request->get('funding', 0);

        //representation for each year
        $progress[] = intval($funding);
        for($i = 1; $i <= $length; $i++){
            $progress[] = intval($funding*pow(1 + $interestRate/100, $i));
        }

        $response = [
            "lastYear" => end($progress),
            "allYears" => $progress,
        ];

        return new Response(json_encode($response), 200);
    }
}