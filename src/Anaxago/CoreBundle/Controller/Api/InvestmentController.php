<?php

namespace Anaxago\CoreBundle\Controller\Api;


use Anaxago\CoreBundle\Entity\Investment;
use Anaxago\CoreBundle\Entity\Project;
use Anaxago\CoreBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class InvestmentController extends Controller
{
    /**
     * Create investment for a user to a project
     *
     * @Route("/api/investment")
     * @Method("POST")
     * @param Request $request
     * @return Response
     */
    public function getAction(Request $request) {

        $errors = array();
        $match = "";

        // post data sent
        $email = $request->request->get('email', '');
        $password = $request->request->get('password', '');
        $projectId = $request->request->get('projectId', 0);
        $amount = $request->request->get('amount', 0);

        // get project
        $projectsRepository = $this->getDoctrine()->getRepository(Project::class);
        $project = $projectsRepository->find($projectId);

        // check user
        $usersRepository = $this->getDoctrine()->getRepository(User::class);
        $user = $usersRepository->findOneBy(array('email' => $email));
        $encoderService = $this->container->get('security.password_encoder');
        if($user){
            $match = $encoderService->isPasswordValid($user, $password);
        }
        if(!$project){
            $errors['project'] = "Missing project";
        }
        if(!$amount){
            $errors['amount'] = "Missing amount";
        }
        if(!$match){
            $errors['login'] = "Wrong email or password";
        }

        if($errors){
            $response = [
                "status" => 500,
                "errors" => $errors
            ];
            return new Response(json_encode($response));
        }

        // addition to database
        $investment = new Investment();
        $investment->setProject($project);
        $investment->setUser($user);
        $investment->setAmount($amount);

        $em = $this->getDoctrine()->getManager();
        $em->persist($investment);
        $em->flush();

        $message = "Marque d'intérêt de ". $amount/100 ."€ pour le projet ".$project->getTitle()." réussi";

        return new Response(json_encode(["message" => $message]), 201);

    }
}